# rj-swim

网站用的vue3 + element-plus完成的，网站中展示了游泳馆的具体内容。然后用nginx做的代理，nginx的代理路径为：/usr/local/nginx/rj-swim-web/
    

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production

```
npm run build
```

### Lints and fixes files
```
npm run lint
```


### 项目实现效果
## **手机端**：

![手机端效果](手机端.png)

### **电脑端**：

![PC端效果](PC端.png)


### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
