import { createApp } from 'vue'
import App from './App.vue'
import Router from './router.js'
import 'font-awesome/css/font-awesome.min.css'  // 使用图标库

// element-ui升级版，支持vue3
import ElementPlus from 'element-plus';
import 'element-plus/theme-chalk/index.css';
import * as ElementPlusIconsVue from '@element-plus/icons-vue'

const app = createApp(App)
app.use(Router).use(ElementPlus)
    .mount('#app')
for (const [key, component] of Object.entries(ElementPlusIconsVue)) {
    app.component(key, component)
}
