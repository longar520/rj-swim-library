import { createRouter, createWebHistory } from 'vue-router'
import Home from "./components/Home.vue"

const routes = [
  {
    path:'/',
    name: '首页',
    component: Home,
    meta: { showMyHeader: true, showMyFooter: true, showFixContinue: true }
  },
  {
    path: '/activity',
    name: '活动消息',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () =>
        import ( /* webpackChunkName: "about" */ './components/views/Activity.vue'),
  },
  {
    path: '/about',
    name: '游泳馆简介',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () =>
        import ( /* webpackChunkName: "about" */ './components/views/About.vue'),
/*    children: [
      {
        path: 'show',
        // path: '/show',
        name: '场馆展示',
        component: () =>
            import ( /!* webpackChunkName: "about" *!/ './components/views/AboutShow.vue'),
      },
    ]*/
  },
  {
    path: '/about/show',
    // path: '/show',
    name: '场馆展示',
    component: () =>
        import ( /* webpackChunkName: "about" */ './components/views/AboutShow.vue'),
  },
  {
    path: '/course',
    name: '课程介绍',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () =>
        import ( /* webpackChunkName: "about" */ './components/views/Courses.vue'),
  },
  {
    path: '/coach',
    name: '师资介绍',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () =>
        import ( /* webpackChunkName: "about" */ './components/views/Coach.vue'),
  },
  {
    path: '/student',
    name: '学员',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () =>
        import ( /* webpackChunkName: "about" */ './components/views/Students.vue'),
  },
  {
    path: '/policy',
    name: '报名须知',
    component: () =>
        import ( /* webpackChunkName: "about" */ './components/views/Poli.vue'),
    children: [
      {
        path: '/rule',
        name: '规章制度',
        component: () =>
            import ( /* webpackChunkName: "about" */ './components/views/Poli.vue'),
      },
      {
        path: '/zscq',
        name: '知识产权',
        component: () =>
            import ( /* webpackChunkName: "about" */ './components/views/Poli.vue'),
      },
      {
        path: '/hzhb',
        name: '合作伙伴',
        component: () =>
            import ( /* webpackChunkName: "about" */ './components/views/Poli.vue'),
      },
      {
        path: '/lxwm',
        name: '联系我们',
        component: () =>
            import ( /* webpackChunkName: "about" */ './components/views/Poli.vue'),
      },

    ]
  },
  {
    path: '/patent',
    name: '可视化数字训练基地',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () =>
        import ( /* webpackChunkName: "about" */ './components/views/Patent.vue'),
    children: [
      {
        path: '/patent/swim',
        name: '游泳光导训练仪',
        component: () =>
            import ( /* webpackChunkName: "about" */ './components/views/Patent.vue'),
      },
      {
        path: '/patent/sport',
        name: '田径光导训练仪',
        component: () =>
            import ( /* webpackChunkName: "about" */ './components/views/Patent.vue'),
      },
      {
        path: '/patent/cardiopulmonary',
        name: '团队心肺功能测试',
        component: () =>
            import ( /* webpackChunkName: "about" */ './components/views/Patent.vue'),
      },
    ]
  },
  {
    path: '/charge',
    name: '收费公示',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () =>
        import ( /* webpackChunkName: "about" */ './components/views/Charge.vue'),
  },
  {
    path: '/serviceStaff',
    name: '服务人员公示',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () =>
        import ( /* webpackChunkName: "about" */ './components/views/ServiceStaff.vue'),
  },
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes,
})

export default router